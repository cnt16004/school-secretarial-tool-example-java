/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import classes.Student;
import classes.Kathigitis;
import gui_package.Gui;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;




/**
 *
 * @author Nick
 */
public class AppMain {
    private final gui_package.Login login;
    private gui_package.Gui frame;

    public Gui getFrame() {
        return frame;
    }
    public static ArrayList<String> mathimata;
    public static ArrayList<classes.Aithousa> aithouses;
    public static ArrayList<String> aithousesMathimaton;
    public static ArrayList<Kathigitis> kathigites;
    public static ArrayList<String> kathigitesMathimaton;
    public static ArrayList<classes.Student> foitites;
    public static DatabaseConnect db;
    public AppMain() throws SQLException{
        db = new DatabaseConnect();
        login = new gui_package.Login(this, this.db);
        login.setVisible(true);
        foitites = new ArrayList<>();
        mathimata = new ArrayList<>();
        mathimata.add("Networking");
        mathimata.add("Super Computers");
        mathimata.add("Hardware Tech");
        mathimata.add("Web Development");
        mathimata.add("Android Development");
        mathimata.add("Cloud");
        mathimata.add("Databases");
        mathimata.add("Algorithms");
        mathimata.add("Java");
        mathimata.add("C++");
        mathimata.add("Boolean Algebra");
        mathimata.add("Thesis");
       
        kathigites = new ArrayList<>();
        kathigites.add(new Kathigitis("Γιώργος", "Αποστόλου", "Κ1Ρ125",this));
        
        kathigitesMathimaton = new ArrayList<>();
        for (int i = 0; i<12; i++){kathigitesMathimaton.add("Δεν έχει οριστεί ακόμη");}
        
        aithouses = new ArrayList<>();
        for (int i = 0; i<1; i++){aithouses.add(new classes.Aithousa("Θαλής",i+1));}
        
        aithousesMathimaton = new ArrayList<>();
        for (int i = 0; i<12; i++){aithousesMathimaton.add("Δεν έχει οριστεί ακόμη");}
        
        foitites.add(new Student("Μπάμπης","Κατρακαλίδης","ΚΜ1407",this));

    
    
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws SQLException {
        new AppMain();
      
    }
    
    //public gui_package.Gui getFrame(){return frame;}
    public void addStudent(String onoma, String epitheto, String am){foitites.add(new Student(onoma,epitheto,am,this));}
    
    
    
}
