/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;
import java.sql.*;

/**
 *
 * @author Nick
 */
public class DatabaseConnect {
    static final String JDBC_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver"; 
    static final String DB_URL = "jdbc:derby:APP;create=true";
    static final String USER = "grammateia";
    static final String PASS = "grammateia";
    static  Connection conn = null;
    static Statement stmt = null;
    static Statement stmt2 = null;

    
    
    public DatabaseConnect() throws SQLException{
       try{
      //STEP 2: Register JDBC driver
      Class.forName(JDBC_DRIVER);
      //STEP 3: Open a connection
      conn = DriverManager.getConnection(DB_URL);

      //STEP 4: Execute a query
      stmt = conn.createStatement();
      stmt2 = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
      
      
      //STEP 6: Clean-up environment
      //stmt.close();
      //conn.close();
         stmt.execute("CREATE TABLE FOITITES("
                                + "id VARCHAR(10) NOT NULL,"
                                + "onoma VARCHAR(15) NOT NULL,"
                                + "epitheto VARCHAR(15) NOT NULL,"
				+ "patros VARCHAR(15) NOT NULL,"
				+ "gennisi VARCHAR(15) NOT NULL,"
				+  "PRIMARY KEY (id)"
				+ ")");
         stmt.execute("CREATE TABLE VATHMOI("
				+ "id_stud VARCHAR(10) NOT NULL,"
				+ "on_math VARCHAR(15) NOT NULL,"
                                + "vathmos INTEGER NOT NULL,"
				+ "PRIMARY KEY (id_stud, on_math),"
                                + "CONSTRAINT ifk1 FOREIGN KEY (id_stud) REFERENCES FOITITES (id),"
                                + "CONSTRAINT ifk2 FOREIGN KEY (on_math) REFERENCES mathimata (onoma)"
				+ ")");
        stmt.execute("CREATE TABLE mathimata("
				+ "onoma VARCHAR(15) NOT NULL,"
				+ "aithousa VARCHAR(15) NOT NULL,"
                                + "kathigitis VARCHAR(10) NOT NULL,"
				+ "PRIMARY KEY (onoma),"
                                + "CONSTRAINT ik1 FOREIGN KEY (kathigitis) REFERENCES KATHIGITES (id)"
				+ ")");
        stmt.execute("CREATE TABLE KATHIGITES("
                                + "id VARCHAR(10) NOT NULL,"
				+ "onoma VARCHAR(15) NOT NULL,"
				+ "epitheto VARCHAR(15) NOT NULL,"
				+  "PRIMARY KEY (id)"
				+ ")");
        stmt.execute("CREATE TABLE USERS("
				+ "username VARCHAR(15) NOT NULL,"
				+ "password VARCHAR(15) NOT NULL,"
				+  "PRIMARY KEY (username)"
				+ ")");
         
      
   }catch(SQLException se){
       
      //Handle errors for JDBC
      se.printStackTrace();
   }catch(Exception e){
      //Handle errors for Class.forName
      e.printStackTrace();
   /*}finally{
      //finally block used to close resources
      try{
         if(stmt!=null)
            stmt.close();
      }catch(SQLException se2){
      }// nothing we can do
      try{
         if(conn!=null)
            conn.close();
      }catch(SQLException se){
         se.printStackTrace();
      }//end finally try*/
   }//end try  
       
  
    }
    public void ExecuteStatement(String query) throws SQLException{stmt.executeUpdate(query);}
    public ResultSet GetResults(String query){ 
        ResultSet rs = null;
        try{
        rs = stmt2.executeQuery(query);
        }catch(SQLException se2){
            se2.printStackTrace();}
    return rs;
    }
}
    

    
