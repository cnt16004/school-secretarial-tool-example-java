
package classes;

/**
 *
 * @author Nick
 */
public class Aithousa {
    private final String name;
    private final Integer number;

    public Aithousa(String name, Integer number) {
        this.name = name;
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public Integer getNumber() {
        return number;
    }
   
    
    
}
