
package classes;

/**
 *
 * @author Nick
 */
public class Kathigitis {
    private final String onoma;
    private final String epitheto;
    private final String am;

    public Kathigitis(String am, String onoma, String epitheto, app.AppMain app) {
        this.onoma = onoma;
        this.epitheto = epitheto;
        this.am = am;
    }

    public String getOnoma() {
        return onoma;
    }

    public String getEpitheto() {
        return epitheto;
    }

    public String getAm() {
        return am;
    }
    
    
}
