/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import app.AppMain;
import java.util.ArrayList;

/**
 *
 * @author Nick
 */
public class Student {
    private final String onoma;
    private final String epitheto;
    private final String am;
    private final ArrayList<String> vathmoi;

    public Student(String onoma, String epitheto, String am, app.AppMain app) {
        this.am = am;
        this.epitheto = epitheto;
        this.onoma = onoma;
        vathmoi = new ArrayList<>();
        for (int i = 0; i<AppMain.mathimata.size(); i++){vathmoi.add("Χωρίς Βαθμό");}
    }
    
    public String getOnoma() {
        return onoma;
    }

    public String getEpitheto() {
        return epitheto;
    }

    public String getAm() {
        return am;
    }

    public ArrayList<String> getVathmoi() {
        return vathmoi;
    }
    

   
    
    
    
    
}
