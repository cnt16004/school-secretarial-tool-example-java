
package gui_package;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Nick
 */
public final class Vathmologio extends javax.swing.JFrame {
    private final Gui3 gui3;
    private final app.AppMain app;
    private final Integer foititisIndex;
    private String title1;
    
    public Vathmologio(app.AppMain app, Gui3 gui3, Integer foititisIndex) throws SQLException {
        this.app = app;
        this.gui3 = gui3;
        initComponents();
        refreshLista();
        this.setLocationRelativeTo(null);
        this.foititisIndex = foititisIndex;
        ResultSet rs = app.db.GetResults("SELECT * FROM FOITITES WHERE id = '" +gui3.getCombo() + "'");
        while (rs.next()){
        title1 = rs.getString("onoma") + " " + rs.getString("epitheto") + " - " + rs.getString("id");}
        jLabel1.setText(title1);
        this.setTitle("Προβολή / Επεξεργασία Βαθμών : " + title1);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jComboBox1 = new javax.swing.JComboBox<>();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        jTextField2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField2ActionPerformed(evt);
            }
        });

        jButton1.setText("Ενημέρωση Βαθμού");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Κλείσιμο");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Προβολή Βαθμού");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("Προβολή Μέσου Όρου");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jLabel1.setText("Φοιτητής");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(76, 76, 76)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTextField2, javax.swing.GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE)
                    .addComponent(jTextField1))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 74, Short.MAX_VALUE)
                        .addComponent(jButton3)
                        .addGap(161, 161, 161))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton1)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(46, 46, 46))
            .addGroup(layout.createSequentialGroup()
                .addGap(127, 127, 127)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton3))
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 153, Short.MAX_VALUE)
                .addComponent(jButton4)
                .addGap(39, 39, 39)
                .addComponent(jButton2)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        ResultSet rs = app.db.GetResults("SELECT * FROM VATHMOI WHERE id_stud = '" + gui3.getCombo() +
                "' AND on_math = '" + jComboBox1.getItemAt(jComboBox1.getSelectedIndex())+ "'");
        boolean hasRows = false;
        try {
            while (rs.next()){
                jTextField1.setText(rs.getString("vathmos"));
            hasRows = true;}
            if(!hasRows){jTextField1.setText("Χωρίς");}
        } catch (SQLException ex) {
            Logger.getLogger(Vathmologio.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(Vathmologio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        
        try {if (Integer.parseInt(jTextField2.getText())>1 && Integer.parseInt(jTextField2.getText())< 11){
            app.db.ExecuteStatement("DELETE FROM VATHMOI WHERE id_stud = '" + gui3.getCombo() + "' AND on_math = '"
            + jComboBox1.getItemAt(jComboBox1.getSelectedIndex()) + "'");
            app.db.ExecuteStatement("INSERT INTO VATHMOI VALUES ('" + gui3.getCombo() + "','" + jComboBox1.getItemAt(jComboBox1.getSelectedIndex()) +
                    "'," + Integer.parseInt(jTextField2.getText()) + ")" );}
        else{JOptionPane.showMessageDialog(this,"Εισάγετε υπαρκτή βαθμολογία");}
        } catch (SQLException ex) {
            Logger.getLogger(Vathmologio.class.getName()).log(Level.SEVERE, null, ex);
        } catch(NumberFormatException ex1)
            {JOptionPane.showMessageDialog(this,"Εισάγετε υπαρκτή βαθμολογία");}
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jTextField2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField2ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        ResultSet rs = app.db.GetResults("SELECT vathmos FROM VATHMOI WHERE id_stud = '" + gui3.getCombo() +"'");
        float add = 0f;
        float result = 0f;
        int i = 0;
        int i2 = 0;
        try {
            while(rs.next()){i = i + 1; add = add + rs.getInt("vathmos");}
            ResultSet rs2 = app.db.GetResults("SELECT onoma FROM MATHIMATA");
            while(rs2.next()){i2 = i2 + 1;}
            if (i>0){result = add/i;}
            JOptionPane.showMessageDialog(this, "O μέσος όρος είναι : " + result + "\n" + 
                    "Έχει περάσει " + i + " μαθήματα \n" + 
                    "Χρωστάει " + (i2-i) + " μαθήματα" );
        } catch (SQLException ex) {
            Logger.getLogger(Vathmologio.class.getName()).log(Level.SEVERE, null, ex);
            
        }catch (IllegalArgumentException ex1){JOptionPane.showMessageDialog(this, "Δεν έχει δώσει ακόμη μαθήματα");}
        try {
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(Vathmologio.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }//GEN-LAST:event_jButton4ActionPerformed

    public static void main(String args[]) {
      
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                
            }
        });
    }
    
    public void refreshLista() throws SQLException{
        ResultSet rs = app.db.GetResults("SELECT onoma FROM MATHIMATA");
        jComboBox1.removeAllItems();
        while(rs.next()){jComboBox1.addItem(rs.getString("onoma"));}
        rs.close();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    // End of variables declaration//GEN-END:variables
}
